## Database Design
https://dbdiagram.io/d/610f74032ecb310fc3c1f22f

### Additional Dependencies
- composer require spatie/laravel-fractal (will give (php artisan make:Transformer))
- redis for Caching (Not used at the time being)
- composer require laravel/passport
- composer require fruitcake/laravel-cors


### Steps to configure passport

 - composer require laravel/passport
 - php artisan passport:install
 - php artisan migrate
 - use HasApiToken trait in User model
 - setup driver as passport in config/auth.php
 - add Passport::routes() in api.php

### Defining Scopes

While asking for scopes, if you want to add multiple scopes you can specify it in a space-separated manner.

If you want all scopes then you can simply specify it as *. 
