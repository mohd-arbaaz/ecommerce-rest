<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /*
     * NOTE: This function is called before any of the other policy methods.
     * If this method returns true then it will not check in the policy method and directly go ahead with true.
     * It works in the same fashion for false.
     * So never return true and false at the same time otherwise the policy method itself will not work,
     */
    public function before(User $user, $ability)
    {
        if($user->isAdmin()) {
            return true;
        }
    }

    public function view(User $authenticatedUser, User $user)
    {
        return $authenticatedUser->id === $user->id;
    }

    public function update(User $authenticatedUser, User $user)
    {
        return $authenticatedUser->id === $user->id;
    }

    public function delete(User $authenticatedUser, User $user)
    {
        return $authenticatedUser->id === $user->id;
    }
}
