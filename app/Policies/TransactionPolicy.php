<?php

namespace App\Policies;

use App\Models\Transaction;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransactionPolicy
{
    use HandlesAuthorization;

    /*
     * NOTE: This function is called before any of the other policy methods.
     * If this method returns true then it will not check in the policy method and directly go ahead with true.
     * It works in the same fashion for false.
     * So never return true and false at the same time otherwise the policy method itself will not work,
     */
    public function before(User $user, $ability)
    {
        if($user->isAdmin()) {
            return true;
        }
    }

    public function view(User $user, Transaction $transaction)
    {
        return $this->isBuyer($user, $transaction) || $this->isSeller($user, $transaction);
    }

    /*
     * NOTE: The below functions should be made in the Transaction model but are made here for understanding purposes.
     */
    private function isBuyer(User $user, Transaction $transaction)
    {
        return $user->id === $transaction->buyer_id;
    }

    private function isSeller(User $user, Transaction $transaction)
    {
        return $user->id === $transaction->product->seller->id;
    }
}
