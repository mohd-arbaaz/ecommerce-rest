<?php

namespace App\Policies;

use App\Models\Seller;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SellerPolicy
{
    use HandlesAuthorization;

    /*
     * NOTE: This function is called before any of the other policy methods.
     * If this method returns true then it will not check in the policy method and directly go ahead with true.
     * It works in the same fashion for false.
     * So never return true and false at the same time otherwise the policy method itself will not work,
     */
    public function before(User $user, $ability)
    {
        if($user->isAdmin()) {
            return true;
        }
    }

    public function view(User $user, Seller $seller)
    {
        return $user->id === $seller->id;
    }

    public function updateProduct(User $user, Seller $seller)
    {
        return $user->id === $seller->id;
    }

    public function deleteProduct(User $user, Seller $seller)
    {
        return $user->id === $seller->id;
    }

    public function sell(User $user, User $seller)
    {
        return $user->id === $seller->id;
    }
}
