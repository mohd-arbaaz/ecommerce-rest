<?php

namespace App\Policies;

use App\Models\Buyer;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BuyerPolicy
{
    use HandlesAuthorization;

    /*
     * NOTE: This function is called before any of the other policy methods.
     * If this method returns true then it will not check in the policy method and directly go ahead with true.
     * It works in the same fashion for false.
     * So never return true and false at the same time otherwise the policy method itself will not work,
     */
    public function before(User $user, $ability)
    {
        if($user->isAdmin()) {
            return true;
        }
    }

    public function view(User $user, Buyer $buyer)
    {
        /*
         * $user comes from the user in the access token
         * $buyer comes as an argument from the middleware applied on the controller.
         * Eg: $this->middleware('can:view,buyer'); (It will not work without passing buyer as an argument)
         */
        return $user->id === $buyer->id;
    }


    public function purchase(User $user, Buyer $buyer)
    {
        return $user->id === $buyer->id;
    }
}
