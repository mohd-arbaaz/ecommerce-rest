<?php

namespace App\Policies;

use App\Models\Product;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    /*
     * NOTE: This function is called before any of the other policy methods.
     * If this method returns true then it will not check in the policy method and directly go ahead with true.
     * It works in the same fashion for false.
     * So never return true and false at the same time otherwise the policy method itself will not work,
     */
    public function before(User $user, $ability)
    {
        if($user->isAdmin()) {
            return true;
        }
    }

    public function addCategory(User $user, Product $product)
    {
        return $user->id === $product->seller->id;
    }

    public function deleteCategory(User $user, Product $product)
    {
        return $user->id === $product->seller->id;
    }
}
