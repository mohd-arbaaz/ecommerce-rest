<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserCreated extends Mailable
{

    public User $user; // public variables are accessible within the associated markdown files.
    /*
     * The command to create a mail is
     * # php artisan make:mail <mailClassName> -m <markdownName>
     * -m is used to create a MarkDown
     * A Markdown is a file in views which consists of the code showing how it will display the mail
     * It is similar to a .md file so md file shortcuts work on it.
     */
    use Queueable, SerializesModels;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
    public function build()
    {
        return $this->markdown('emails.welcome')->subject('Verify Account!');
    }
}
