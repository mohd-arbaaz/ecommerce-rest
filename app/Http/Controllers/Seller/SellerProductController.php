<?php


namespace App\Http\Controllers\Seller;


use App\Http\Controllers\ApiController;
use App\Models\Product;
use App\Models\Seller;
use App\Models\User;
use App\Transformers\ProductTransformer;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SellerProductController extends ApiController
{
    public function __construct()
    {
        $this->middleware('transform.input:' . ProductTransformer::class)->only('store', 'update');
        $this->middleware('auth:api');
        $this->middleware('scope:manage-product')->except('index');

        $this->middleware('can:view,seller')->only('index');
        $this->middleware('can:sell,seller')->only('store');
        // NOTE: method name is in camel case and middleware name is in snake case.
        $this->middleware('can:update-product,seller')->only('update');
        $this->middleware('can:delete-product,seller')->only('delete');
    }
    public function index(Seller $seller) {
        if(\request()->user()->tokenCan('read-general') || \request()->user()->tokenCan('manage-product')) {
            $products = $seller->products;
            return $this->showAll($products);
        }
        throw new AuthorizationException("Invalid Scopes!");
    }

    public function store(Request $request, User $seller) {
        /**
         * create a rule where ek endpoint call hoga jahan product banega
         * uske success pe ek dusra endpoint call hoga jo product ko category se map karega
         * a product is made available only when it is mapped to atleast 1 category
         */

        $rules = [
            'name' => 'required',
            'description' => 'required',
            'quantity' => 'required|integer|min:1',
            'image' => 'required|image'
        ];

        $this->validate($request, $rules);
        $data = $request->all();

        $data['status'] = Product::UNAVAILABLE_PRODUCT;
        $data['image'] = $request->image->store(''); // blank string diye coz usko path required hai bt humne already filesystems.php mei default path set kiye hai.
        $data['seller_id'] = $seller->id;

        $product = Product::create($data);
        return $this->showOne($product);
    }

    public function update(Request $request, Seller $seller, Product $product) {
        /**
         * REST error dega agar kisi ne update ka endpoint call kiya bt update nhi hua toh
         */

        $this->verifySeller($seller, $product);

        $rules = [
            'name' => 'min:1',
            'description' => 'min:5',
            'quantity' => 'integer|min:1',
            'status' => 'in:' . Product::AVAILABLE_PRODUCT . ',' . Product::UNAVAILABLE_PRODUCT,
            'image' => 'image'
        ];

        $this->validate($request, $rules);
        $product->fill(
            $request->only([
                'name',
                'description',
                'quantity'
            ])
        );

        if($request->has('status')) {
            $product->status = $request->status;

            if($product->isAvailable() && $product->categories()->count() == 0) {
                return $this->errorResponse('Product must be associated at least one category to be available', 409);
            }
        }

        if($request->hasFile('image')) {

            Storage::delete($product->image);
            $product->image = $request->image->store('');
        }

        if($product->isClean()) {
            return $this->errorResponse("You have not updated any value!", 422);
        }
        $product->save();
        return $this->showOne($product);
    }

    public function destroy(Seller $seller, Product $product) {
        $this->verifySeller($seller, $product);
        Storage::delete($product->image);
        $product->delete();
        return $this->showOne($product);
    }

    private function verifySeller(Seller $seller, Product $product) {
        if($seller->id != $product->seller_id) {
            throw new HttpException(422, "You are trying to update someone else's product!");
        }
    }
}
