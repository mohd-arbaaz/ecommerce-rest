<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;

class CacheResponser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $url = request()->url();
        $queryParameters = request()->query();
        $method = request()->getMethod();

        ksort($queryParameters);

        $queryString = http_build_query($queryParameters); // PHP Function

        $fullUrl = "$method:{$url}?{$queryString}";

        if(Cache::has($fullUrl)) {
            /*
             * Bug Fix: This was returning mixed where a response was meant to be returned. So additional modifications to the response was not possible.
             */
            return new Response(Cache::get($fullUrl));
        }
        return $next($request);
    }
}
