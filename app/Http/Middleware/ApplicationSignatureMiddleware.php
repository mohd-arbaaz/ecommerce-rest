<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ApplicationSignatureMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, string $header = 'X-Name') // X- se start hone wale headers are not understandable by http. It is standard way of writing headers.
    {
        /*
         * Before middleware processes the request first and then calls next
         * After middleware calls next and processes the response after that.
         */
        $response = $next($request);
        $response->headers->set($header, config('app.name'));
        return $response;
    }
}
