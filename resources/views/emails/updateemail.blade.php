@component('mail::message')
# Hello, {{ $user->name }}

Your email has been updated, please verify it by clicking on the following link.

If you face any technical issues, you can contact support at support@ecommercerest.com

@component('mail::button', ['url' => route('users.verify', $user->verification_token)])
Verify Email
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
